﻿using System;

namespace comp5002_10013041_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //----------------------------------------------------------------
            // Tssk 4
            //----------------------------------------------------------------
            // Author       : Sing Graajae Cho (Korean Name : Shinkee Cho)
            // Student-id   : 10013041
            // E-mail       : i486you@gmail.com
            // Created Date : 20/Mar/2017
            // Description  : This code is an assignment for 'COMP5002 Intro to Programming' in Toi-Ohomai.
            //                The concept is to imagine that we are running our own shop and make up the name,
            //                and to welcome a virtual visitor who can buy maximum 2 products by just typing in product price as number.
            //                Afterwards, display the total price such as subtotal, GST, grand total.
            //----------------------------------------------------------------

            Console.Clear();

            //--------------------------------
            // 1. Print a line that welcomes the user to your program (which is your shop) (10%)
            //--------------------------------

            // "MAGZOG" is the name of my shop.
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("            Welcome to MAGZOG");
            Console.WriteLine();
            Console.WriteLine(" \"You will find beyond your imagination\"");
            Console.WriteLine("-----------------------------------------");
            // In order to distinguish between questions.
            Console.Write("\n\n\n");


            //--------------------------------
            // 2. Declare all your variables that you will for the program (10%)
            //--------------------------------
            
            // Declares variables
            string customerName;            // Customer's name
            double productPrice;            // The number which customer type in will be assigned. (Ex. 32.54)
            string answer;                  // Any answer will be assigned to this variable by typing in about questions.
            double percentageOfGST;         // Percentage of GST (Ex. 15 for 15%)
            double amountOfGST;             // Amount of GST
            double subTotal, grandTotal;    // Total number(In my case, total of produnt price)

            // Initializes variables
            percentageOfGST = 15.00;        //  GST : 15%
            productPrice = 0.00;
            amountOfGST = 0.00;
            subTotal = grandTotal = 0.00;   //  Init price : $0.00



            //--------------------------------
            // 3. Ask the user for their name and store it in a variable (10%)
            //--------------------------------

            // Prints out a question for customer's name
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine(" What is your name?");
            Console.WriteLine("-----------------------------------------");
            Console.Write("> Please type in your name and press enter key : ");
            
            // Receives the customer's name through keyboard, and assings the name to the variable 'customerName'.
            customerName = Console.ReadLine();
            
            // (Optional) Print out the customer's name.
            Console.Write($"> Hi~ {customerName} \n\n");


            //--------------------------------
            // 4. Ask the user to type in a number with 2 decimal places (10%)
            //--------------------------------

            // Prints out a question about product price.
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine(" How much is the product selected?");
            Console.WriteLine("-----------------------------------------");
            Console.Write("> Please type in the price with 2 decimal places and press enter key. ex) 34.15 : ");

            // Receives the number for product price.
            answer = Console.ReadLine();

            // We need convert the variable 'answer' from string to number(double).
            // Simple Code : productPrice = double.Parse(answer);
            // (Optional) In order to prevent error, Check the answer string if it is numeric string, and convert to double.
            productPrice = ConvertStringToDouble(answer);
            // (Optional) I use Math.Round() method to prevent that the number is calculated as three decimal places in.
            productPrice = Math.Round(productPrice, 2);

            // Sub-total price should be the same as the first product price at this moment.
            subTotal = productPrice; 

            // (Optional) Print out the fist number again.
            Console.Write("> The price of your first product is {0:C2}\n\n", productPrice);


            //--------------------------------
            // 5. Ask the user to type in an answer for a decision (10%)
            //--------------------------------

            // Prints out a question about adding another product price.
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine(" Do you want to add an another product(price)?");
            Console.WriteLine("-----------------------------------------");
            Console.Write("> Please type in the answer as 'yes' or 'no' and press enter key : ");
            
            // Receives the answer as 'yes' or 'no'.
            answer = Console.ReadLine().ToLower();

            // In order to distinguish between questions.
            Console.Write("\n\n");



            //--------------------------------
            // 6. Ask the user if they want to add another number (30% (20 + 10 below))
            //--------------------------------

            // 6-1. If they answer yes - allow them to add another number (20%)
            //--------------------------------
            if(answer == "yes" || answer == "y")
            {
                // Prints out a question about the another product price selected.
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine(" How much is another product selected?");
                Console.WriteLine("-----------------------------------------");
                Console.Write("> Please type in the price with 2 decimal places and press enter key. ex) 34.15 : ");

                // Receives the product price as string.
                answer = Console.ReadLine();

                // Converts the price from string to number.
                // Simple Code : productPrice = double.Parse(answer);
                // (Optional) In order to prevent error, Check the answer string if it is numeric string, and convert to double.
                productPrice = ConvertStringToDouble(answer);
                // (Optional) I use Math.Round() method to prevent that the number is calculated as three decimal places in.
                productPrice = Math.Round(productPrice, 2);

                // Sums all price of products.
                subTotal = subTotal + productPrice;

                // (Optional) Print out the second number again.
                Console.Write("> The price of your second product is {0:C2} \n\n", productPrice);

                // Print out the sub-total price.
                Console.WriteLine("\n\n");
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine(" Sub Total   : {0:C2}", subTotal);
            }

            // 6-2. If the answer no - then show them the total of the number (10%)
            //--------------------------------
            else
            {
                // (Optional)
                // If the answer is not 'yes' or 'y', it will be reconized as 'no'.
                // And print out what happened
                if(answer != "no" && answer != "n")
                {
                    Console.WriteLine("-----------------------------------------");
                    Console.WriteLine(" !!! : You typed in wrong answer.");
                    Console.WriteLine("-----------------------------------------");
                    Console.WriteLine($"> You answered with \"{answer}\".");
                    Console.WriteLine($"> In this case, we think you answered with 'no'.");
                }

                // Print out the sub-total price.
                Console.WriteLine("\n\n");
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine(" Sub Total   : {0:C2}", subTotal);
            }



            //--------------------------------
            // 7. Once the decision has finished at the GST to the total number and show the total amount on the screen. (10%)
            //--------------------------------

            // Calculates amount of GST and prints out and prints out.
            // (Optional) I use Math.Round() method to prevent that the number is calculated as three decimal places in.
            amountOfGST = subTotal * (percentageOfGST / 100);
            amountOfGST = Math.Round(amountOfGST, 2);
            Console.WriteLine(" GST (15%)   : {0:C2}", amountOfGST);


            // Calculates amount of Grand Total and prints out.
            // (Optional) I use Math.Round() method to prevent that the number is calculated as three decimal places in.
            grandTotal = subTotal + amountOfGST;
            amountOfGST = Math.Round(amountOfGST, 2);
            Console.WriteLine(" Grand Total : {0:C2}", grandTotal);
            Console.WriteLine("-----------------------------------------");
            // In order to distinguish between questions.
            Console.WriteLine("\n\n");


            //--------------------------------
            // 8. Write on the screen “Thank you for shopping with us, please come again” (10%)
            //--------------------------------
            Console.WriteLine(" ** Thank you for shopping with us, please come again **");
            Console.Write("\n\n");
        }


        //
        // (Optional)
        // This method is optional.
        // If you do not want to get error by unexpected an answer, you can use this method.
        // Because expected answer is a numeric string, but a customer can type in just string.
        //
        static double ConvertStringToDouble(string numericStr)
        {
            double number = 0.00;

            // If the string is double number.
            // Return the number.
            if(double.TryParse(numericStr, out number))
            {
                return number;
            }
            // Else, print out error message and exit program.
            else 
            {
                // Error message
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine(" !!! : You typed in wrong number.");
                Console.WriteLine(" !!! : Sorry! We can't solve it right now.");
                Console.WriteLine(" !!! : Bye~!!");
                Console.WriteLine("-----------------------------------------\n\n");

                System.Environment.Exit(1);
                return 0;
            }
        }
    }
}
