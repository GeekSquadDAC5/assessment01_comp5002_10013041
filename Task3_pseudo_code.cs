﻿using System;

namespace comp5002_10013041_assessment01
{
    class PseudoCode
    {
        static void pseudoCode()
        {
/*
            //----------------------------------------------------------------
            // Tssk 3 : Pseudocode
            //----------------------------------------------------------------
            // Author       : Sing Graajae Cho (Korean Name : Shinkee Cho)
            // Student-id   : 10013041
            // E-mail       : i486you@gmail.com
            // Created Date : 20/Mar/2017
            // Description  : This pseudo code is an assignment for 'COMP5002 Intro to Programming' in Toi-Ohomai.
            //                The concept is to imagine that we are running our own shop and make up the name,
            //                and to welcome a virtual visitor who can buy maximum 2 products by just typing in product price as number.
            //                Afterwards, display the total price such as sub-total, GST, grand-total.
            //
            //----------------------------------------------------------------
            // How to write a string
            //----------------------------------------------------------------
            // I used single or double qoutation mark to express a string
            // ex) "Hello World", 'Good morning'
            // 
            //----------------------------------------------------------------
            // Used pseudo functions/methods
            //----------------------------------------------------------------
            // 
            // 1.
            // Functiona/methods       : string receiveInputData()
            // Description of function : This function receive string from input device such as keyboard.
            // Type of input argument  : none
            // Type of return          : string
            // 
            // 2.
            // Function name           : double convertStringToDouble(string answer)
            // Description of function : This function convert string to number(double).
            // type of input argument  : string
            // type of return          : double
            //----------------------------------------------------------------

            Console.Clear();

            //--------------------------------
            // 1. Print a line that welcomes the user to your program (which is your shop) (10%)
            //--------------------------------

            // "Magzog" is the name of my shop.
            print "Welcome to Magzog";



            //--------------------------------
            // 2. Declare all your variables that you will for the program (10%)
            //--------------------------------
            
            // Declares variables
            string customerName;
            double productPrice;
            string answer;
            integer percentageOfGST;
            double amountOfGST;
            double subTotal, grandTotal;

            // Init variables
            percentageOfGST = 15;   //  GST : 15%



            //--------------------------------
            // 3. Ask the user for their name and store it in a variable (10%)
            //--------------------------------

            // Prints out a question for customer's name
            print "What is your name? (Please type in your name and press enter key) : ";
            
            // Receives the customer's name through keyboard, and assings the name to the variable 'customerName'.
            // (Refer to the comments at the top of this file for instructions about this pseudo function.)
            customerName = receiveInputData();



            //--------------------------------
            // 4. Ask the user to type in a number with 2 decimal places (10%)
            //--------------------------------

            // Prints out a question about product price.
            print "How much is the product selected? (Please type in the price with 2 decimal places and press enter key) ex) 34.15 :   ";

            // But the method or function of receive input data which is supported by any lanuage library usually return back value as string
            // !!! : the variable 'answer' contatin number(double) as string.
            // (Refer to the comments at the top of this file for instructions about this function.)
            answer = receiveInputData();    // This function return a string that typed in by keyboard.

            // So we need convert the variable 'answer' from string to number.
            // (Refer to the comments at the top of this file for instructions about this pseudo function.)
            productPrice = convertStringToDouble(answer);

            // Sub-total price should be the same as the first product price at this moment.
            subTotal = productPrice;



            //--------------------------------
            // 5. Ask the user to type in an answer for a decision (10%)
            //--------------------------------

            // Prints out a question about adding another product price.
            print "Do you want to add an another product(price)? (Please type in the answer as 'yes'  or 'no' and press enter key) : ";
            
            // Receives the answer as 'yes' or 'no'.
            // (Refer to the comments at the top of this file for instructions about this pseudo function.)
            answer = receiveInputData();



            //--------------------------------
            // 6. Ask the user if they want to add another number (30% (20 + 10 below))
            //--------------------------------

            // 6-1. If they answer yes - allow them to add another number (20%)
            //--------------------------------
            if(answer == "yes" or "y")
            {
                // Prints out a question about the another product price selected.
                print "How much is another product selected? (Please type in the price with 2 decimal places and press enter key) ex) 34.15 : ";

                // Receives the product price as string.
                answer = receiveInputData();

                // Converts the price from string to number.
                productPrice = convertStringToDouble(answer);

                // Sums all price of products.
                subTotal = subTotal + productPrice;

                // Print out sub-total price
                print "SubTotal : ";
                print subTotal;
            }

            // 6-2. If the answer no - then show them the total of the number (10%)
            //--------------------------------
            else
            {
                // Print out sub-total price
                print "SubTotal : ";
                print subTotal;
            }



            //--------------------------------
            // 7. Once the decision has finished at the GST to the total number and show the total amount on the screen. (10%)
            //--------------------------------

            // Calculates amount of GST and prints out.
            amountOfGST = subTotal * (percentageOfGST / 100);
            print "GST (15%) : ";
            print amountOfGst;

            // Calculates amount of Grand Total and prints out.
            grandTotal = subTotal + amountOfGST;
            print "Grand Total : ";
            print grandTotal;



            //--------------------------------
            // 8. Write on the screen “Thank you for shopping with us, please come again” (10%)
            //--------------------------------
            print "Thank you for shopping with us, please come again";
*/
        }
    }
}
